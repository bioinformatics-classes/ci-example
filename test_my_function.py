#!/usr/bin/env python3

import pytest
import my_function


def test_square_number():
    """
    Tests if square_number() really is squaring a number
    """
    known_inputs = [2, 4, 6]
    expected_outputs = [4, 16, 36]

    for i, j in zip(known_inputs, expected_outputs):
        assert my_function.square_number(i) == j

#!/usr/bin/env python3

def square_number(number):
    """
    Squares a number.
    """
    square = number * number
    return square
